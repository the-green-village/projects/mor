# MOR  
This repository contains the scripts necessary to download and
produce the values to the kafka platform. Both scripts are python
scripts that are ran using shell scripts called by a cronjob. 

## apirequest
This script runs every half hour and uses the credentials specified
in projectsecrets.py to obtain an apikey that is valid for an hour.
This api key is then saved in a file called apikey.py, after which it
can be used in the real_time script.

## real_time
This script requests, downloads, groups, and tags the data to be produced.
It does this in multiple steps:
1. Obtain the asset_id, only for the setup matching the name "Installaties prototype MOR TUDelft"
2. Fetch the metadata for the variables
3. Fetch the variable summary
4. Fetch the historic data of all the variables for a specified timeframe.
5. Combine the historic data with the metadata to make create the values
   that need to be produced.

This script does a batch request for 50 variables at a time to speed up the
request. In total, this script takes about a minute to complete. Most of this
time is spent waiting on the requestst to be done. 
